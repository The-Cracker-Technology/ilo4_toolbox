rm -rf /opt/ANDRAX/ilo4_toolbox

cp -Rf scripts /opt/ANDRAX/ilo4_toolbox

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy scripts... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd scanner

go build -o iloscan iloscan.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build iloscan... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip iloscan

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf iloscan /opt/ANDRAX/bin

cp -Rf ../andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
